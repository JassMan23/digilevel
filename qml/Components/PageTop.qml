import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2

Rectangle {
    id: pageTop
    height: _pageTitle.height * 2 ; width: parent.width

    property int pageTitleId: 0
    onPageTitleIdChanged: {
        switch ( pageTitleId ) {
            case 1: {
                _pageTitle.text = i18n.tr("Notes")
                _m_note.visible = false
                break;
            }
            case 2: {
                _pageTitle.text = i18n.tr("Settings")
                _m_set.visible = false
                break;
            } 
            case 3: {
                _pageTitle.text = i18n.tr("Help")
                _m_help.visible = false
                break;
            } 
            case 4: {
                _pageTitle.text = i18n.tr("About")
                _m_about.visible = false
                break;
            } 
            case 5: {
                _pageTitle.text = i18n.tr("Accelerometers")
//                _m_accel.visible = false
                break;
            } 

        }
        console.log("===Page: " + _pageTitle.text + " ====")

    }

    Rectangle {
        height: _pageTitle.height ; width: parent.width//units.gu(50)
        anchors.fill: parent
        anchors.margins: units.gu(1)

        RowLayout {
            width: parent.width
            Rectangle { 
                Layout.preferredWidth: pageTop.width / 16
                ToolButton {
                    text: " ❬ "
                    font.pointSize: 32
                    font.weight: Font.ExtraLight
                    onClicked: mainStack.pop()
                }
            }
            Rectangle { 
                //Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: pageTop.width / 2
                Label {
                    id: _pageTitle
                    text: ""
                    font.pointSize: 40
                }
            }
            Rectangle { 
                Layout.preferredWidth: pageTop.width / 16
                Layout.alignment: Qt.AlignRight
                ToolButton {
                    text: "≡" //≣☰
                    font.pointSize: 36
                    font.weight: Font.DemiBold
                    onClicked: _menu.open()
                    Menu {
                        id: _menu
                        MenuItem {
                            text: "  ≡"
                            font.pointSize: 36
                        }
                        MenuItem {
                            id: _m_note
                            text: " ✍ "
                            font.pointSize: 44
                            onTriggered: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('../NotesPage.qml'))}
                        }
                        MenuItem {
                            id: _m_set
                            text: " ⚙ "
                            font.pointSize: 44
                            onTriggered: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('../SettingsPage.qml'))}
                        }
                        MenuItem {
                            id: _m_help
                            text: " ❔ "
                            font.pointSize: 44
                            onTriggered: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('../HelpPage.qml'))}
                        }
                        MenuItem {
                            id: _m_about
                            text: " 🛈 "
                            font.pointSize: 56
                            onTriggered: {mainStack.pop() ; mainStack.push(Qt.resolvedUrl('../AboutPage.qml'))}
                        }
                   }
                }
            }
        }
    }
    Rectangle {
        height: units.dp(2) ; width: parent.width
        anchors.bottom: parent.bottom
        color: 'lightgray'
    }
}
