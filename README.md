# DigiLevel

DigiLevel is a digital level with a semi analog UI. It was originally based on the mhall116 demo for Ubuntu Touch but has been expanded, rewritten, rehashed and generally messed around with. Some code from the original on launchpad possibly still exists. Some code is from  examples in the QMLCreator for Android/iOS by Oleg Yadrov (olegyadrov) and 
Alfred E. Neumayer (fredldotme).

This a rewrite of the [Level finder app by mhall119 and others on the UBPorts team](https://github.com/sverzegnassi/ubuntu-touch-level-app).

[Get Level Finder on the OpenStore.](https://open-store.io/app/level.ubports)

Additional code from examples in [QMLCreator by freldotme and olegyanrov](https://github.com/fredldotme/qmlcreator)

## Contributions

A huge thank you to those predecessors above.

## Development

Build and run using [clickable](https://github.com/bhdouglass/clickable).

## Translations

Translation help is welcome

## Donate

If you like DigiLevel, please donate to UBPorts to help improve the OS.

## License

Copyright (C) 2019 David Julien-Waring 'JassMan23'

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
